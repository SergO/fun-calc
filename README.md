Fun Calc
=====================

**Functional Calculator** — a pure function written in Scala, which takes a simple math expression such as " (1 - 3) * (-2 - 3) / 1000 " as a string and evaluates it to a real number.

### What is this repository for? ###

It's an example how string can be parsed as a math expression in a pure functional style.

In order to self-practice, parser converts string into binary tree model which is presented in the MOOC course ["Functional Programming Principles in Scala, Lecture 4.6 — Pattern Matching"](https://www.coursera.org/learn/progfun1/lecture/cdHAM/lecture-4-6-pattern-matching) by prof. Martin Odersky. That way, this repo could by used as an additional learning materials.

An example of using in practice:
[Lazy Calc](http://lazycalc.sergo.eu)
— Fun Calc meets HTML5 Boilerplate via Scala.js

### Requirements ###

– Scala (2.12)

### How to use ###

– as application:
```bash
$ cd path/to/application
$ sbt
> run " (1 - 3) * (-2 - 3) / 1000 "

[info] Running funcalc.FunCalcApp  (1 - 3) * (-2 - 3) / 1000
0.01
```

<br/>

– as scala Object:
```bash
$ cd path/to/application
$ sbt
> console
scala> 100 * funcalc.FunCalc(" (1 - 3) * (-2 - 3) / 1000 ")

res0: Double = 1.0
```