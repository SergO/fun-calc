package funcalc

/**
  * Created by SergO on 8.3.17.
  */
class EvalSpec extends UnitSpec {
  object `An Expr` {
    object `When evaluated` {
      def `Number(246) should evaluates to 246`() {
        assert(Eval(Number(246)) == 246)
      }
      def `Sum(Number(12), Number(34)) should evaluates to 46`() {
        assert(Eval(Sum(Number(12), Number(34))) == 46)
      }
      def `Sub(Number(12), Number(34)) should evaluates to -22`() {
        assert(Eval(Sub(Number(12), Number(34))) == -22)
      }
      def `Mul(Number(2), Number(4)) should evaluates to 8`() {
        assert(Eval(Mul(Number(2), Number(4))) == 8)
      }
      def `Div(Number(2), Number(4)) should evaluates to 0.5`() {
        assert(Eval(Div(Number(2), Number(4))) == 0.5)
      }
      def `2*3+4=10 Sum(Mul(Number(2), Number(3)), Number(4)) should evaluates to 10`() {
        assert(Eval(Sum(Mul(Number(2), Number(3)), Number(4))) == 10)
      }
      def `2+3*4=14 Sum(Number(2), Mul(Number(3), Number(4))) should evaluates to 14`() {
        assert(Eval(Sum(Number(2), Mul(Number(3), Number(4)))) == 14)
      }
    }
  }
}
