package funcalc

import org.scalatest

/**
  * Created by SergO on 8.3.17.
  */
class FunCalcSpec extends UnitSpec {

  object `An incorrect string expression` {
    object `When parsed` {
      def `a [CalcExceptionEmpty] should be thrownBy Calc("")`() {
        assertThrows[CalcExceptionEmpty] { FunCalc("") }
      }
      def `a [CalcExceptionIllegalSymbol] should be thrownBy Calc("1+text")`() {
        assertThrows[CalcExceptionIllegalSymbol] { FunCalc("1 + text") }
      }
      def `a [CalcExceptionMissingLeftOperand] should be thrown`() {
        assertThrows[CalcExceptionMissingLeftOperand] { FunCalc("2*(+3)") }
        assertThrows[CalcExceptionMissingLeftOperand] { FunCalc("*2*(4+3)") }
        assertThrows[CalcExceptionMissingLeftOperand] { FunCalc("--2") }
      }
      def `a [CalcExceptionMissingRightOperand] should be thrown`() {
        assertThrows[CalcExceptionMissingRightOperand] { FunCalc("2*(5+)") }
        assertThrows[CalcExceptionMissingRightOperand] { FunCalc("(4+3)*") }
      }
      def `a [CalcExceptionTwoNumbersInSuccession] should be thrown`() {
        assertThrows[CalcExceptionTwoNumbersInARow] { FunCalc("2*(5+10 15)") }
        assertThrows[CalcExceptionTwoNumbersInARow] { FunCalc("(4+3)*2 2") }
      }
      def `a [CalcExceptionTwoOperatorsInSuccession] should be thrown`() {
        assertThrows[CalcExceptionTwoOperatorsInARow] { FunCalc("2**(5+10)") }
        assertThrows[CalcExceptionTwoOperatorsInARow] { FunCalc("(4+3)**2") }
        assertThrows[CalcExceptionTwoOperatorsInARow] { FunCalc("2--2") }
      }
      def `a [CalcExceptionTooManyOpeningBrackets] should be thrown`() {
        assertThrows[CalcExceptionTooManyOpeningBrackets] { FunCalc("2*(5+(10+4)") }
        assertThrows[CalcExceptionTooManyOpeningBrackets] { FunCalc("((4+3)*2") }
      }
      def `a [CalcExceptionTooManyClosingBrackets] should be thrown`() {
        assertThrows[CalcExceptionTooManyClosingBrackets] { FunCalc("2*(5+10+4))") }
        assertThrows[CalcExceptionTooManyClosingBrackets] { FunCalc("(4+3)*2)") }
      }
    }
  }

  object `A string expression` {
    object `When parsed` {
      def `"246" should evaluates to 246`() {
        assert(FunCalc("246") == 246)
      }
      def `"12+34" should evaluates to 46`() {
        assert(FunCalc("12+34") == 46)
        assert(FunCalc("12 + 34") == 46)
        assert(FunCalc(" 12 + 34 ") == 46)
      }
      def `"12+34+56" should evaluates to 102)`() {
        assert(FunCalc("12 + 34 + 56") == 102)
      }
      def `"2*4" should evaluates to 8`() {
        assert(FunCalc("2 * 4") == 8)
      }
      def `"2*3+4" should evaluates to 10`() {
        assert(FunCalc("2 * 3 + 4") == 10)
      }
      def `"2+3*4" should evaluates to 14`() {
        assert(FunCalc("2 + 3 * 4") == 14)
      }
      def `"5*2+3*4" should evaluates to 22`() {
        assert(FunCalc("5 * 2 + 3 * 4") == 22)
      }
      def `"5+2*3+4" should evaluates to 15`() {
        assert(FunCalc("5 + 2 * 3 + 4") == 15)
      }

      def `"7*(2+3)" should evaluates to 35`() {
        assert(FunCalc("7*(2+3)") == 35)
      }
      def `"(2+3)*7" should evaluates to 35`() {
        assert(FunCalc("(2+3)*7") == 35)
      }
      def `"(2+3)*(3+4)" should evaluates to 35`() {
        assert(FunCalc("(2+3)*(3+4)") == 35)
      }

      def `"2*(2+3*(4*(2+1)+2))" should evaluates to 88`() {
        assert(FunCalc("2*(2+3*(4*(2+1)+2))") == 88)
      }
      def `"2*(2+3*{4*[2+1]+2})" should evaluates to 88`() {
        assert(FunCalc("2*(2+3*{4*[2+1]+2})") == 88)
      }

      def `"-2" should evaluates to -2`() {
        assert(FunCalc("-2") == -2)
      }
      def `"-(2+3*3)+5" should evaluates to -6`() {
        assert(FunCalc("-(2+3*3)+5") == -6)
      }
      def `"(1-2)*(-5+3)" should evaluates to 2`() {
        assert(FunCalc("(1-2)*(-5+3)") == 2)
      }
      def `"-(-1-2)*(-(-5-3))" should evaluates to 24`() {
        assert(FunCalc("-(-1-2)*(-(-5-3))") == 24)
      }

      def `"6/3" should evaluates to 2`() {
        assert(FunCalc("6/3") == 2)
      }
      def `"5/2" should evaluates to 2`() {
        assert(FunCalc("5/2") == 2.5)
      }
    }

  }
}
