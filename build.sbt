ThisBuild / scalaVersion := "2.13.4"

lazy val root = project.in(file(".")).
  aggregate(funCalc.js, funCalc.jvm).
  settings(
    publish := {},
    publishLocal := {},
  )

lazy val funCalc = crossProject(JSPlatform, JVMPlatform).in(file(".")).
  settings(
    name := "Fun Calc",
    version := "1.2",
  ).
  jvmSettings(
    // Add JVM-specific settings here
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.2",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.2" % "test"
  ).
  jsSettings(
    // Add JS-specific settings here
    scalaJSUseMainModuleInitializer := true,
  )
