package funcalc

/**
  * Created by SergO on 11.4.17.
  */
object Eval {
  def apply(e: Expr): Double = eval(e)

  def eval(e: Expr): Double = e match {
    case Number(n) => n
    case Sum(e1, e2) => eval(e1) + eval(e2)
    case Sub(e1, e2) => eval(e1) - eval(e2)
    case Mul(e1, e2) => eval(e1) * eval(e2)
    case Div(e1, e2) => eval(e1) / eval(e2)
  }
}
