package funcalc

/**
  * Created by sergo on 10.3.17.
  */
object FunCalcApp {
  def main(args: Array[String]): Unit = {

    val result = if (args.length > 0) {
      try {
        FunCalc(args(0))
      } catch {
        case e: CalcException => "Error: " + e.message
        case _: Exception => "Error: Unknown error"
      }
    }
    else {
      "Error: A string math expression is expected as a parameter"
    }

    println(result)
  }
}
