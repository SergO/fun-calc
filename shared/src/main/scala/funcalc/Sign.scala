package funcalc

/**
  * Created by SergO on 17.3.17.
  */
trait Sign {
}

case class SignNumber(value: Double) extends Sign

case class SignOperator(value: String) extends Sign

case class SignBracket(value: String) extends Sign
