package funcalc

/**
  * Created by SergO on 7.3.17.
  */
trait Expr {}

case class Number(n: Double) extends Expr

case class Sum(e1: Expr, e2: Expr) extends Expr

case class Sub(e1: Expr, e2: Expr) extends Expr

case class Mul(e1: Expr, e2: Expr) extends Expr

case class Div(e1: Expr, e2: Expr) extends Expr
