package funcalc

/**
  * Created by SergO on 23.3.17.
  */
trait CalcException extends Exception {
  val cause: Throwable = null
  val message: String
}

case class CalcExceptionIllegalSymbol(symbol: String) extends CalcException {
  val message: String = "Illegal symbol `%s`".format(symbol)
}
case class CalcExceptionEmpty() extends CalcException {
  val message: String = "Expression is empty"
}
case class CalcExceptionMissingRightOperand() extends CalcException {
  val message: String = "Missing right operand"
}
case class CalcExceptionMissingLeftOperand() extends CalcException {
  val message: String = "Missing left operand"
}
case class CalcExceptionTwoNumbersInARow(n: Double, m: Double) extends CalcException {
  val message: String = "Two numbers in a row: %f, %f".format(n, m)
}
case class CalcExceptionTwoOperatorsInARow(op1: String, op2: String) extends CalcException {
  val message: String = "Two operators in a row: %s, %s".format(op1, op2)
}
case class CalcExceptionTooManyOpeningBrackets() extends CalcException {
  val message: String = "Too many opening brackets"
}
case class CalcExceptionTooManyClosingBrackets() extends CalcException {
  val message: String = "Too many closing brackets"
}
