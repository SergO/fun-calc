package funcalc

import scala.annotation.tailrec

/**
  * Created by SergO on 7.3.17.
  */
object FunCalc {
  type SignExpr = List[Sign]

  def apply(raw_expr: String, lazyMode: Boolean = false): Double = {

    /**
      * Parses a "human readable" math expression such as " (1 - 3) * (-2 - 3) / 1000 "
      * into a sequence of `Sign` case classes (SignNumber | SignOperator | SignBracket)
      * */
    def primaryParser(raw_expr: String, num_acc: String, e: SignExpr): SignExpr = raw_expr.take(1) match {
      case "" => num_acc match {
        case "" => e
        case _ => SignNumber(num_acc.toDouble) :: e
      }
      case ch if "1234567890.".contains(ch) => primaryParser(raw_expr.tail, num_acc + ch, e)
      case ch if "+-*/".contains(ch) => num_acc match {
        case "" => SignOperator(ch) :: primaryParser(raw_expr.tail, "", e)
        case _ => SignNumber(num_acc.toDouble) :: SignOperator(ch) :: primaryParser(raw_expr.tail, "", e)
      }
      case ch if "[]{}()".contains(ch) => if (lazyMode) {
        // lazy mode, ignoring brackets
        num_acc match {
          case "" => primaryParser(raw_expr.tail, "", e)
          case _ => SignNumber(num_acc.toDouble) :: primaryParser(raw_expr.tail, "", e)
        }
      } else {
        num_acc match {
          case "" => SignBracket(ch) :: primaryParser(raw_expr.tail, "", e)
          case _ => SignNumber(num_acc.toDouble) :: SignBracket(ch) :: primaryParser(raw_expr.tail, "", e)
        }
      }
      // ignoring
      case ch if " \n".contains(ch) => num_acc match {
        case "" => primaryParser(raw_expr.tail, "", e)
        case _ => SignNumber(num_acc.toDouble) :: primaryParser(raw_expr.tail, "", e)
      }
      case ch =>
        throw CalcExceptionIllegalSymbol(ch)
    }

    /**
      * Recursively gets rid of brackets in a sequence of `Sign`
      * */
    def bracketsParser(signExpr: SignExpr): SignExpr = {
      type stackExpr = (SignBracket, SignExpr)

      @tailrec
      def acc(signExpr: SignExpr, stack: List[stackExpr]): SignExpr = signExpr match {
        case List() => stack.tail match {
          case List() => stack.head._2.reverse

          // final stack must contain only one expression
          case _ =>
            throw CalcExceptionTooManyOpeningBrackets()
        }
        case sign :: signExprTail => sign match {
          case SignBracket(bracketSign) => bracketSign match {
            case oBracket if "{[(".contains(oBracket) =>
              val newStackExpr = (SignBracket(oBracket), List())
              acc(signExprTail, newStackExpr :: stack)

            case cBracket =>
              val currStackExpr = stack.head
              val evaluatedExpr = SignNumber(signExprEval(currStackExpr._2.reverse))

              // checking stack's previous expressions
              stack.tail match {
                // expression in brackets cannot be first in stack
                case List() =>
                  throw CalcExceptionTooManyClosingBrackets()
                case prevStackExpr :: stackTailTail =>
                  val updatedPrevStackExpr =
                    (prevStackExpr._1, evaluatedExpr :: prevStackExpr._2)

                  acc(signExprTail, updatedPrevStackExpr :: stackTailTail)
              }

          }
          case notBracket =>
            val currStackExpr = stack.head
            val updatedCurrStackExpr = (currStackExpr._1, sign :: currStackExpr._2)
            acc(signExprTail, updatedCurrStackExpr :: stack.tail)
        }
      }

      val initStackExpr = (SignBracket(""), List())

      acc(signExpr, initStackExpr :: List())
    }

    /**
      * Evaluates sequence of `Sign` to Double
      * through creating binary tree `Expr`
      * and evaluating that via `Eval` object
      * */
    def signExprEval(signExpr: SignExpr): Double = {
      def acc(signExpr: SignExpr): Expr = signExpr match {
        case List() =>
          throw CalcExceptionEmpty()
        case op1 :: signExprTail => op1 match {
          case SignOperator("-") => signExprTail match {
            case SignNumber(operandForNegating) :: signExprTailTail =>
              acc(SignNumber(-operandForNegating) :: signExprTailTail)
            case _ =>
              throw CalcExceptionMissingLeftOperand()
          }
          case SignOperator(_) =>
            throw CalcExceptionMissingLeftOperand()
          case SignNumber(rightOperand) => signExprTail match {
            case List() => Number(rightOperand)
            case operator :: signExprTailTail => operator match {
              case SignNumber(unexpectedOperand) =>
                throw CalcExceptionTwoNumbersInARow(rightOperand, unexpectedOperand)
              case SignOperator(opSign) => signExprTailTail match {
                case List() =>
                  throw CalcExceptionMissingRightOperand()
                case op2 :: signExprTailTailTail => op2 match {
                  case SignOperator(unexpectedOperator) =>
                    throw CalcExceptionTwoOperatorsInARow(opSign, unexpectedOperator)
                  case SignNumber(leftOperand) => opSign match {
                    case "+" => if (lazyMode) {
                      acc(SignNumber(Eval(Sum(Number(rightOperand), Number(leftOperand)))) :: signExprTailTailTail)
                    } else {
                      Sum(Number(rightOperand), acc(op2 :: signExprTailTailTail))
                    }
                    case "-" =>  if (lazyMode) {
                      acc(SignNumber(Eval(Sub(Number(rightOperand), Number(leftOperand)))) :: signExprTailTailTail)
                    } else {
                      Sub(Number(rightOperand), acc(op2 :: signExprTailTailTail))
                    }
                    case "*" =>
                      acc(SignNumber(Eval(Mul(Number(rightOperand), Number(leftOperand)))) :: signExprTailTailTail)
                    case "/" =>
                      acc(SignNumber(Eval(Div(Number(rightOperand), Number(leftOperand)))) :: signExprTailTailTail)
                  }
                }
              }
            }
          }
        }
      }

      Eval(acc(signExpr))
    }

    val signExpr = primaryParser(raw_expr, "", List())
    val simpleSignExpr = bracketsParser(signExpr)
    signExprEval(simpleSignExpr)
  }

}
